local formspec =
		'size[8,9]'..
        default.gui_bg..
        default.gui_bg_img..
        default.gui_slots..
        'image_button[3.5,1;2,2;ui_sun_icon.png;button;]'..
        'list[current_name;fuel;2.25,1.5;1,1]'..
        'image[2.25,1.5;1,1;default_mese_crystal_transparent.png]'..
        'list[current_player;main;0,4.25;8,1;]'..
        'list[current_player;main;0,5.5;8,3;8]'..
        'listring[current_player;main]'..
        'listring[current_player;main]'..
        'listring[current_name;fuel]'..
        'listring[current_player;main]'


minetest.register_node('daymachine:daymachine', {
	description = 'Day Machine',
	tiles = {"daymachine_daymachine_front.png^ui_sun_icon.png"},
    use_texture_alpha = true,
	paramtype2        = 'facedir',
	is_ground_content = false,
	groups            = {cracky=2},
	sounds            = default.node_sound_stone_defaults(),

	can_dig = can_dig,

    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string('formspec', inactive_formspec)
		local inv = meta:get_inventory()
		inv:set_size('fuel', 1)
        meta:set_string('formspec', formspec)
	end,

	on_receive_fields = function(pos, formname, fields, player)
		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end
		local meta = minetest.get_meta(pos)
        local inv  = meta:get_inventory()

        if(fields.button) then 
			if inv:contains_item('fuel', 'default:mese_crystal') then
			    inv:set_stack('fuel', 1, '')
			    minetest.set_timeofday((6000 % 24000) / 24000)
				return
			end
		end
    end,

    allow_metadata_inventory_put = allow_metadata_inventory_put,
	allow_metadata_inventory_move = allow_metadata_inventory_move,
	allow_metadata_inventory_take = allow_metadata_inventory_take,
})

minetest.register_craft({
	output = "daymachine:daymachine",
	recipe = {
		{"basic_materials:plastic_sheet", "basic_materials:energy_crystal_simple", "basic_materials:plastic_sheet"},
		{"basic_materials:heating_element", "default:mese", "basic_materials:heating_element"},
		{"basic_materials:plastic_sheet", "basic_materials:energy_crystal_simple", "basic_materials:plastic_sheet"}
	}
})